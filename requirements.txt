# Requirements for the Heroku install via pip.
numpy >=0.13.0
scipy >=1.0
matplotlib
sympy
cython
fastcache
nbformat
jupyter_core
plotly
flask
flask-compress
dash
dash-renderer
dash-html-components
dash-core-components
gunicorn
